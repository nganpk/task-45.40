$(document).ready(function(){
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  // Mảng userObjects là mảng chứa dữ liệu user. Từng phần tử là object
  var gUserDb = {
    userObjects: [
        {
          id: 2,
          username: "quannv",
          firstname: "Ngo Van",
          lastname: "Quan",
          age: 18,
          email: "quan@gmail.com",
          roleId: 5
        },
        {
          id: 3,
          username: "longdh",
          firstname: "Do Hoang",
          lastname: "Long",
          age: 19,
          email: "long@gmail.com",
          roleId: 8
        },
        {
          id: 4,
          username: "hiendt",
          firstname: "Do Thi",
          lastname: "Hien",
          age: 29,
          email: "hien@gmail.com",
          roleId: 11
        },
        {
          id: 6,
          username: "lanht",
          firstname: "Ho Thi",
          lastname: "Lan",
          age: 27,
          email: "lan@gmail.com",
          roleId: 13
        }
    ],
    filterUser: function(paramFilterObj){
      var vFilterResult = [];
      vFilterResult = this.userObjects.filter(function(paramUser){
        return (paramFilterObj.roleId === "" || paramUser.roleId == paramFilterObj.roleId)
      })
      return vFilterResult
    },
    addNewUser: function(paramUserData){
      this.userObjects.push(paramUserData); 
    },
    editUser: function(paramUserData){
      var vUserIndex = getIndexFormUserId(paramUserData.id);
      this.userObjects.splice(vUserIndex, 1, paramUserData);
    },
    deleteUser: function(paramUserId){
      var vUserIndex = getIndexFormUserId(paramUserId);
      this.userObjects.splice(vUserIndex, 1);
    }
  }
  //Mảng dữ liệu role
  var gRoleDb = {
    roleObjects: [
        {
          roleId: 5,
          roleName: "Admin" 
        },
        {
          roleId: 8,
          roleName: "Manager" 
        },
        {
          roleId: 11,
          roleName: "Teacher" 
        },
        {
          roleId: 13,
          roleName: "Staff" 
        },
  ]
  };

  //tạo biến chứa số thứ tự
  var gSTT = 1;
  //tạo biến chứa id
  var gUserId = [];
  //Các trạng thái CRUD
  var gFORM_MODE_NORMAL = "Normal";
  var gFORM_MODE_INSERT = "Insert";
  var gFORM_MODE_UPDATE = "Update";
  var gFORM_MODE_DELETE = "Delete";
  // biến toàn cục cho trạng thái của form: mặc định ban đầu là trạng thái Normal
  var gFormMode = gFORM_MODE_NORMAL;


  //tạo biến chứa các cột của bảng
  const gUSER_DATA_COLS = ["stt", "username", "firstname", "lastname", "age", "email","roleId","action"];
  //tạo thứ tự các cột
  const gUSER_STT_COL = 0;
  const gUSERNAME_COL = 1;
  const gFIRSTNAME_COL = 2;
  const gLASTNAME_COL = 3;
  const gAGE_COL = 4;
  const gEMAIL_COL = 5;
  const gROLEID_COL = 6;
  const gACTION_COL = 7;

  //tạo bảng
  var gUserTable = $('#user-table').DataTable({
      columns:[
        {data: gUSER_DATA_COLS[gUSER_STT_COL]},
        {data: gUSER_DATA_COLS[gUSERNAME_COL]},
        {data: gUSER_DATA_COLS[gFIRSTNAME_COL]},
        {data: gUSER_DATA_COLS[gLASTNAME_COL]},
        {data: gUSER_DATA_COLS[gAGE_COL]},
        {data: gUSER_DATA_COLS[gEMAIL_COL]},
        {data: gUSER_DATA_COLS[gROLEID_COL]},
        {data: gUSER_DATA_COLS[gACTION_COL]},
      ],
      columnDefs:[
        { // định nghĩa lại cột STT
          targets: gUSER_STT_COL,
          render: function() {
            return gSTT ++;
        }
        },
        { // định nghĩa lại cột action
          targets: gACTION_COL,
          defaultContent: `
            <i class="far fa-edit edit-user" style="width: 20px;cursor:pointer;"></i>
            <i class="fas fa-trash-alt delete-user" style="width: 20px;cursor:pointer;"></i>
          `
        },
        {//định nghĩa lại cột role
          targets: gROLEID_COL,
          render: function(data){
            for(var bI = 0; bI < gRoleDb.roleObjects.length; bI ++){
              if(data == gRoleDb.roleObjects[bI].roleId){
                return gRoleDb.roleObjects[bI].roleName;
              }
            }
          }
        }
      ]
  });
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageLoading();
  //gán sự kiện cho nút lọc dữ liệu
  $('#btn-filter-data').on('click', function(){
    onBtnFilterDataClick();
  })
  //gán sự kiện cho nút thêm mới user
  $('#btn-add-user').on('click',function(){
    onBtnAddNewUserClick();
  })
  //gán sự kiện cho icon edit
  $('#user-table').on('click', '.edit-user', function(){
    onIconEditClick(this);
  })
  //Gán sự kiện cho nút save data
  $('#btn-save-data').on('click', function(){
    onBtnSaveDataClick();
  })
  //Gán sự kiện cho icon delete
  $('#user-table').on('click', '.delete-user', function(){
    onIconDeleteClick(this);
  })
  //gán sự kiện cho nút confirm delete
  $('#btn-confirm').on('click',onBtnDeleteUserClick);
  //reset dữ liệu khi click nút cancel
  $('#user-modal').on('hidden.bs.modal', function () {
    resetUserData();
  })

  //reset dữ liệu khi click nút cancel
  $('#delete-modal').on('hidden.bs.modal', function () {
    resetUserData();
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //Hàm xử lý sự kiện load trang
  function onPageLoading(){
    "use strict";
    //cập nhật trạng thái bình thường
    $('#div-form-mod').html(gFormMode);
    //load data user vào bảng
    loadUserDataToTable(gUserDb.userObjects);
    //load role vào select
    loadRoleToSelect();
    //load role vào select trong modal
    loadRoleToModalSelect();
  }

  //Hàm xử lý sự kiện lọc dữ liệu
  function onBtnFilterDataClick(){
    "use strict";
    //tạo đối tượng
    var vFilterObject = {
      roleId:""
    }
    //Thu thập dữ liệu
    vFilterObject.roleId = $('#select-role').val();
    //Lọc dữ liệu
    var vUserFilterResult = gUserDb.filterUser(vFilterObject);
    //Hiện dữ liệu lọc lên bảng
    loadUserDataToTable(vUserFilterResult);
  }

  //Hàm xử lý sự kiện thêm mới user
  function onBtnAddNewUserClick(){
    "use strict";
    //cập nhật trạng thái INSERT
    gFormMode = gFORM_MODE_INSERT;
    $('#div-form-mod').html(gFormMode);
    //Hiện modal
    $('#user-modal').modal('show');
  }

  //Hàm xử lý sự kiện ấn icon edit
  function onIconEditClick(paramIconEdit){
    "use strict";
    //cập nhật trạng thái update
    gFormMode = gFORM_MODE_UPDATE
    $('#div-form-mod').html(gFormMode);
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramIconEdit).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gUserTable.row(vRowSelected); 
    //Lấy data của dòng 
    var vUserData = vDatatableRow.data();      
    //lấy id
    gUserId = vUserData.id
    console.log(gUserId);
    //Hiện modal
    $('#user-modal').modal('show');
    //Hiện dữ lên modal
    showUserDataToModal(vUserData);
  }

  //Hàm xử lý sự kiện ấn nút save data
  function onBtnSaveDataClick(){
    "use strict";
    //tạo đối tượng
    var vUserData = {
      id: 0,
      username: "",
      firstname: "",
      lastname: "",
      age: "",
      email: "",
      roleId: ""
    }
    //Thu thập dữ liệu
    getUserData(vUserData);
    //validate dữ liệu
    var vIsValidated = validateData(vUserData);
    if(vIsValidated){
      saveUserData(vUserData);
      //load data vào bảng
      loadUserDataToTable(gUserDb.userObjects);
      //thông báo khi cập nhật dữ liệu thành công
      alert("Cập nhật dữ liệu thành công");
      //tắt modal sau khi thêm thành công
      $("#user-modal").modal("hide");
      //reset dữ liệu
      resetUserData();
    }
  }
  //Hàm xử lý sự kiện ấn icon delete
  function onIconDeleteClick(paramIconDelete){
    "use strict";
    //cập nhật trạng thái delete
    gFormMode = gFORM_MODE_DELETE
    $('#div-form-mod').html(gFormMode);
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramIconDelete).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gUserTable.row(vRowSelected); 
    //Lấy data của dòng 
    var vUserData = vDatatableRow.data();      
    //lấy id
    gUserId = vUserData.id
    //Hiện modal
    $('#delete-modal').modal('show');
  }
  //hàm xử lý sự kiện ấn nút delete
  function onBtnDeleteUserClick(){
    "use strict";
    gUserDb.deleteUser(gUserId);
    //hiển thị kết quả
    loadUserDataToTable(gUserDb.userObjects);
    //thông báo
    alert("Xóa dữ liệu thành công");
    // ẩn modal form delete
    $("#delete-modal").modal("hide");
    //reset dữ liệu
    resetUserData();
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //hàm xử lý sự kiện load user data vào table
  function loadUserDataToTable(paramUserArr){
    "use strict";
    gSTT = 1;
    gUserTable.clear();
    gUserTable.rows.add(paramUserArr);
    gUserTable.draw();
  }

  //hàm load role vào select
  function loadRoleToSelect(){
    "use strict";
    var vSelectRole = $('#select-role');
    for(var bI = 0; bI < gRoleDb.roleObjects.length; bI ++){
      vSelectRole.append($('<option>', {
          value: gRoleDb.roleObjects[bI].roleId,
          text: gRoleDb.roleObjects[bI].roleName
      }));
    }
  }
  //Hàm load role vào select trong modal
  function loadRoleToModalSelect(){
    "use strict";
    var vSelectRole = $('#select-role-modal');
    for(var bI = 0; bI < gRoleDb.roleObjects.length; bI ++){
      vSelectRole.append($('<option>', {
          value: gRoleDb.roleObjects[bI].roleId,
          text: gRoleDb.roleObjects[bI].roleName
      }));
    }
  }
  //hàm xử lý sự kiện hiện dữ liệu lên modal
  function showUserDataToModal(paramRowData){
    "use strict";
    $('#inp-username').val(paramRowData.username);
    $('#inp-fname').val(paramRowData.firstname);
    $('#inp-lname').val(paramRowData.lastname);
    $('#inp-email').val(paramRowData.email);
    $('#inp-age').val(paramRowData.age);
    $('#select-role-modal').val(paramRowData.roleId);
  }
  //Hàm thêm id vào user
  function getNextId(){
    "use strict";
    var vNextId = 0;
    // nếu mảng chưa có phần tử nào, thì id sẽ bắt đầu từ 1
    if(gUserDb.userObjects.length == 0) {
      vNextId = 1;
    }
    else { // id tiếp theo bằng id của phần tử cuối cùng cộng thêm 1
      vNextId = gUserDb.userObjects[gUserDb.userObjects.length - 1].id + 1;
    }
    return vNextId;
  }
  //hàm thu thập dữ liệu
  function getUserData(paramInfo){
    "use strict";
    if (gFormMode === gFORM_MODE_INSERT) {//thêm mới
      paramInfo.id = getNextId();
    } 
    else {//sửa
      paramInfo.id = gUserId;
    }
    paramInfo.username = $('#inp-username').val().trim();
    paramInfo.firstname = $('#inp-fname').val().trim();
    paramInfo.lastname = $('#inp-lname').val().trim();
    paramInfo.age = $('#inp-age').val().trim();
    paramInfo.email = $('#inp-email').val().trim();
    paramInfo.roleId = $('#select-role-modal').val();
  }

  //Hàm validate dữ liệu
  function validateData(paramInfo){
    "use strict";
    //validate username
    if(paramInfo.username === ""){
      alert('Xin vui lòng nhập username');
      return false;
    }
    //kiểm tra trùng username
    if(isExistUsername(paramInfo.username)){
      alert('Username bị trùng');
      return false;
    }
    // validate firstname
    if(paramInfo.firstname === ""){
      alert('Xin vui lòng nhập firstname');
      return false;
    }
    // validate lastname
    if(paramInfo.lastname === ""){
      alert('Xin vui lòng nhập lastname');
      return false;
    }

    //validate email
    var vValidEmail = validateEmail(paramInfo.email);
    if(!vValidEmail){
      alert('Xin vui lòng nhập email');
      return false;
    }
    if(isExistEmail(paramInfo.email)){
      alert('Email bị trùng');
      return false;
    }
    //validate age
    if(isNaN(paramInfo.age) || paramInfo.age < 10 || paramInfo.age > 180){
      alert('Xin vui lòng nhập age (10 ~ 180)');
      return false;
    }

    //validate role
    if(paramInfo.roleId === ""){
      alert('Xin vui lòng chọn role');
      return false;
    }
    return true;
  }

  //hàm kiểm tra username có bị trùng hay không
  function isExistUsername(paramUsername){
    "use strict";
    var vFound = false;
    var vIndex = 0;
    if ( gFormMode ===  gFORM_MODE_INSERT) {//thêm mới => không được phép trùng với cái đã có
      while (!vFound && vIndex < gUserDb.userObjects.length) {
        if (gUserDb.userObjects[vIndex].username === paramUsername) {
          vFound = true;          
        } else {
          vIndex ++;
        }
      }
    }
    else {//sửa => không được phép trùng với những dòng khác và được phép trùng với dòng hiện tại 
      while (!vFound && vIndex < gUserDb.userObjects.length){
        if (gUserDb.userObjects[vIndex].username === paramUsername && gUserDb.userObjects[vIndex].id != gUserId ) {
          vFound = true;          
        } else {
          vIndex ++;
        }
      }
      
    }

    return vFound;
  }
  //Hàm validate email
  function validateEmail(paramEmail) {
    "use strict";
    var vFormat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if ( ! vFormat.test(paramEmail) ) {
          return false;
      }
      return true;
  }
  //Hàm kiểm tra email có bị trùng hay không
  function isExistEmail(paramEmail){
    "use strict";
    var vFound = false;
    var vIndex = 0;
    if (gFormMode ===  gFORM_MODE_INSERT) {//thêm mới => không được phép trùng với cái đã có
      while (!vFound && vIndex < gUserDb.userObjects.length) {
        if (gUserDb.userObjects[vIndex].email === paramEmail) {
          vFound = true;          
        } else {
          vIndex ++;
        }
      }
    } else {//sửa => không được phép trùng với những dòng khác và được phép trùng với dòng hiện tại 
      while (!vFound && vIndex < gUserDb.userObjects.length){
        if (gUserDb.userObjects[vIndex].email === paramEmail && gUserDb.userObjects[vIndex].id != gUserId) {
          vFound = true;          
        } else {
          vIndex ++;
        }
      }
      
    }

    return vFound;
  }
  //Hàm tìm index của user
  function getIndexFormUserId(paramUserId) {
    "use strict";
    var vUserIndex = -1;
    var vUserFound = false;
    var vLoopIndex = 0;
    while(!vUserFound && vLoopIndex < gUserDb.userObjects.length) {
      if(gUserDb.userObjects[vLoopIndex].id === paramUserId) {
        vUserIndex = vLoopIndex;
        vUserFound = true;
      }
      else {
        vLoopIndex ++;
      }
    }
    return vUserIndex;
  }
  //hÀM SAVE DATA
  function saveUserData(paramUserData){
    "use strict";
    if (gFormMode === gFORM_MODE_INSERT) {//thêm mới => thêm luôn vào cuối mảng
      gUserDb.addNewUser(paramUserData);

    } 
    else {//sửa => cập nhật vào đúng vị trí của user trong mảng
      gUserDb.editUser(paramUserData);
    }
  }

  //Hàm reset dữ liệu
  function resetUserData(){
    "use strict";
    //resest trạng thái
    gFormMode = gFORM_MODE_NORMAL;
    $("#div-form-mod").html(gFormMode);

    //reset voucherid
    gUserId = 0;

    //reset modal form
    $('#inp-username').val("");
    $('#inp-fname').val("");
    $('#inp-lname').val("");
    $('#inp-age').val("");
    $('#inp-email').val("");
    $('#select-role-modal').val("");
    
  }


})
  